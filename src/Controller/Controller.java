package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import Model.Hashingcode;
import View.GUI;

public class Controller{

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller();
	}

	public Controller() {
		frame = new GUI();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		
		String url1 = "www.google.com";
		String url2 = "www.yahop.com";
		String url3 = "www.gamemuns.com";
		String url4 = "www.sanookz.com";
		String url5 = "www.board.ini3s.co.th";
		String url6 = "www.msns.com";
		String url7 = "www.comandmore.com";
		String url8 = "www.bittorrent.com";
		String url9 = "www.cakeroll.com";
		String url10 = "www.chaingmai.com";
		String url11 = "www.learningcenter.com";
		String url12 = "www.esport.com";

		Hashingcode inputstring = new Hashingcode();
		
		inputstring.setName(url1);
		int showcode01 = inputstring.hashCode(url1);
		int modfour01 = inputstring.modNumber(showcode01);
		frame.setResult("1. "+inputstring.toString()+ "  :  " +showcode01+ "  : mod = "+modfour01);
		
		inputstring.setName(url2);
		int showcode02 = inputstring.hashCode(url2);
		int modfour02 = inputstring.modNumber(showcode02);
		frame.extendResult("2. "+inputstring.toString()+ "  :  " +showcode02+ "  : mod = "+modfour02);
		
		inputstring.setName(url3);
		int showcode03 = inputstring.hashCode(url3);
		int modfour03 = inputstring.modNumber(showcode03);
		frame.extendResult("3. "+inputstring.toString()+ "  :  " +showcode03+ "  : mod = "+modfour03);
		
		inputstring.setName(url4);
		int showcode04 = inputstring.hashCode(url4);
		int modfour04 = inputstring.modNumber(showcode04);
		frame.extendResult("4. "+inputstring.toString()+ "  :  " +showcode04+ "  : mod = "+modfour04);
		
		inputstring.setName(url5);
		int showcode05 = inputstring.hashCode(url5);
		int modfour05 = inputstring.modNumber(showcode05);
		frame.extendResult("5. "+inputstring.toString()+ "  :  " +showcode05+ "  : mod = "+modfour05);
		
		inputstring.setName(url6);
		int showcode06 = inputstring.hashCode(url6);
		int modfour06 = inputstring.modNumber(showcode06);
		frame.extendResult("6. "+inputstring.toString()+ "  :  " +showcode06+ "  : mod = "+modfour06);
		
		inputstring.setName(url7);
		int showcode07 = inputstring.hashCode(url7);
		int modfour07 = inputstring.modNumber(showcode07);
		frame.extendResult("7. "+inputstring.toString()+ "  :  " +showcode07+ "  : mod = "+modfour07);
		
		inputstring.setName(url8);
		int showcode08 = inputstring.hashCode(url8);
		int modfour08 = inputstring.modNumber(showcode08);
		frame.extendResult("8. "+inputstring.toString()+ "  :  " +showcode08+ "  : mod = "+modfour08);
		
		inputstring.setName(url9);
		int showcode09 = inputstring.hashCode(url9);
		int modfour09 = inputstring.modNumber(showcode09);
		frame.extendResult("9. "+inputstring.toString()+ "  :  " +showcode09+ "  : mod = "+modfour09);
		
		inputstring.setName(url10);
		int showcode010 = inputstring.hashCode(url10);
		int modfour010 = inputstring.modNumber(showcode010);
		frame.extendResult("10. "+inputstring.toString()+ "  :  " +showcode010+ "  : mod = "+modfour010);
		
		inputstring.setName(url11);
		int showcode011 = inputstring.hashCode(url11);
		int modfour011 = inputstring.modNumber(showcode011);
		frame.extendResult("11. "+inputstring.toString()+ "  :  " +showcode011+ "  : mod = "+modfour011);
		
		inputstring.setName(url12);
		int showcode012 = inputstring.hashCode(url12);
		int modfour012 = inputstring.modNumber(showcode012);
		frame.extendResult("12. "+inputstring.toString()+ "  :  " +showcode012+ "  : mod = "+modfour012);
		
	}

	ActionListener list;
	GUI frame;
}
