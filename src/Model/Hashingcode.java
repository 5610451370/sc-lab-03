package Model;

public class Hashingcode {
	
	private String name;
	private String inputstring;
	private int inputint;


	public int hashCode(String inputstring){
		int ascii = 0;
		for(char c: inputstring.toCharArray()){
			char character = c;
			ascii = ascii+(int)character;
		}
		return ascii;
	}
	
	public int modNumber(int inputint){
		int modnaja = 0;
		modnaja = modnaja+(inputint%4);
		return modnaja;
	}

	public void setName(String str){
		name = str;
	}
	
	public String toString() {
		return name;
	}
}

